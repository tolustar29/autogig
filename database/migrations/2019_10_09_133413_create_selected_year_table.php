<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSelectedYearTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('selected_year', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 20);
			$table->integer('model_id')->unsigned()->index('selected_year_model_id_foreign');
			$table->integer('brand_id')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('selected_year');
	}

}
