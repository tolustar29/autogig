<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQlEditorImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ql_editor_images', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('page_id')->nullable();
			$table->string('page_type', 100)->nullable()->default('default');
			$table->text('url', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ql_editor_images');
	}

}
