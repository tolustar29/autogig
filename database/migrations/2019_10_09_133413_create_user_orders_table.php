<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_orders', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('order_code', 191)->nullable();
			$table->text('name', 65535)->nullable();
			$table->string('email', 191)->nullable();
			$table->string('state', 191)->nullable();
			$table->string('lga', 191)->nullable();
			$table->text('address', 65535)->nullable();
			$table->string('phone_number', 191)->nullable();
			$table->string('tracking_status', 191)->nullable();
			$table->string('shipping', 191)->nullable();
			$table->string('price', 191)->nullable();
			$table->string('total_price', 191)->nullable();
			$table->string('normal_product_count', 50)->nullable();
			$table->string('custom_product_count', 50)->nullable();
			$table->string('payment', 191)->nullable()->default('NO');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_orders');
	}

}
