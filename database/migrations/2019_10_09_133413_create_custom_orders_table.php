<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('vin', 80);
			$table->string('brand', 80);
			$table->string('model', 80);
			$table->date('year_selected');
			$table->string('trim', 80);
			$table->string('part_number', 80)->nullable();
			$table->string('part_name', 80)->nullable();
			$table->integer('quantity');
			$table->text('comment', 65535)->nullable();
			$table->text('media', 65535)->nullable();
			$table->string('cart', 80)->nullable();
			$table->float('price', 10, 0)->nullable()->default(0);
			$table->float('total', 10, 0)->nullable()->default(0);
			$table->string('order_code', 40)->nullable();
			$table->string('payment_status', 120)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom_orders');
	}

}
