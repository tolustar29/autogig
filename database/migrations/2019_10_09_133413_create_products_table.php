<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index('products_category_id_foreign');
			$table->string('title', 100);
			$table->text('slug', 65535)->nullable();
			$table->text('specification', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->float('price', 10, 0);
			$table->string('media_id', 100)->nullable();
			$table->string('featured', 20)->nullable();
			$table->string('hot', 20)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
