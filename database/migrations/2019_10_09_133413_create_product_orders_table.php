<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index('orders_product_id_foreign');
			$table->text('product_title', 65535)->nullable();
			$table->text('slug', 65535)->nullable();
			$table->integer('quantity');
			$table->string('cart', 100)->nullable();
			$table->float('price', 10, 0)->nullable();
			$table->float('total', 10, 0)->nullable();
			$table->text('media', 65535)->nullable();
			$table->string('order_code', 40)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_orders');
	}

}
