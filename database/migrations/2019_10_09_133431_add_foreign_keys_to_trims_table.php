<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTrimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trims', function(Blueprint $table)
		{
			$table->foreign('selected_year_id')->references('id')->on('selected_year')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trims', function(Blueprint $table)
		{
			$table->dropForeign('trims_selected_year_id_foreign');
		});
	}

}
