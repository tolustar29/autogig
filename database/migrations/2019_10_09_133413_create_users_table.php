<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->nullable();
			$table->string('email', 100);
			$table->string('password', 100);
			$table->string('forgot_password', 100)->nullable();
			$table->string('role', 20)->nullable();
			$table->string('phone_number', 21)->nullable();
			$table->string('address', 150)->nullable();
			$table->string('state', 80)->nullable();
			$table->string('country', 80)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
