<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSelectedYearTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('selected_year', function(Blueprint $table)
		{
			$table->foreign('model_id')->references('id')->on('models')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('selected_year', function(Blueprint $table)
		{
			$table->dropForeign('selected_year_model_id_foreign');
		});
	}

}
