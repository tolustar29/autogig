<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trims', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 100);
			$table->string('slug', 120)->nullable();
			$table->integer('selected_year_id')->unsigned()->index('trims_selected_year_id_foreign');
			$table->integer('model_id')->unsigned();
			$table->integer('brand_id')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trims');
	}

}
