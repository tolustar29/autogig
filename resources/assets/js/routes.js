import Vue from 'vue'
import VueRouter from 'vue-router'
import VueProgressBar from 'vue-progressbar'
import jwt_decode from 'jwt-decode'
Vue.use(VueRouter);

import swal from 'sweetalert2'
import store from './store/index';

import axios from 'axios';

import GridLoader from "vue-spinner/src/GridLoader.vue";
Vue.component("grid-loader", GridLoader);

import StarRating from 'vue-star-rating';
Vue.component('star-rating', StarRating);

import VueCountdownTimer from 'vuejs-countdown-timer'
Vue.use(VueCountdownTimer)

import Tawk from 'vue-tawk';
  
Vue.use(Tawk, {
    tawkSrc: 'https://embed.tawk.to/5c407c8aab5284048d0d60cd/default'
});
Vue.prototype.Tawk = Tawk;



window.primaryColor = "#3a5294";


window.toast = swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 5000
});

window.popup = swal.mixin({
  toast: false,
  showConfirmButton: true,
});


Vue.use(VueProgressBar, {
  color: 'red',
  failedColor: 'red',
  thickness: '9px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 400
  },
  autoFinish: false,
})


import Home from './views/pages/Home'
import Login from './views/pages/Login'
import ChangePassword from './views/pages/ChangePassword'
import ResetPassword from './views/pages/ResetPassword'
import MakePayment from './views/pages/MakePayment'
import VerifyNewsletterEmail from './views/pages/VerifyNewsletterEmail'
import NewsLetter from './views/pages/NewsLetter'
import LogOut from './views/pages/LogOut'
import Redirect from './views/pages/Redirect'
import Register from './views/pages/Register'
import About from './views/pages/About'
import Terms from './views/pages/Terms'
import PrivacyPolicy from './views/pages/PrivacyPolicy'
import BillingPolicy from './views/pages/BillingPolicy'
import AutogigTv from './views/pages/AutogigTv'
import TrainingCertification from './views/pages/TrainingCertification'
import FleetManagement from './views/pages/FleetManagement'
import SafetyTips from './views/pages/SafetyTips'
import FAQ from './views/pages/FAQ'
import BuyParts from './views/pages/BuyParts'
import AddToBrand from './views/pages/AddToBrand'
import Contact from './views/pages/Contact'
import Products from './views/pages/Products'
import ProductPage from './views/pages/ProductPage'
import CustomProductPage from './views/pages/CustomProductPage'
import Cart from './views/pages/Cart'
import Checkout from './views/pages/Checkout'
import MyOrders from './views/pages/MyOrders'
import UserProfile from './views/pages/UserProfile'
import Search from './views/pages/Search'
import NotFound from "./views/pages/NotFound";



import AdminIndex from "./views/pages/admin/Index";
import AdminDashboard from "./views/pages/admin/Dashboard";
import ProductCategories from "./views/pages/admin/ProductCategories";
import AdminProducts from "./views/pages/admin/Products";
import AdminProductsByCategory from "./views/pages/admin/ProductsByCategory";
import AddProduct from "./views/pages/admin/AddProduct";
import EditProduct from "./views/pages/admin/EditProduct";
import OrderPage from "./views/pages/admin/OrderPage";
import EditOrderPage from "./views/pages/admin/EditOrderPage";
import AdminReviews from "./views/pages/admin/Reviews";
import ReviewsByProduct from "./views/pages/admin/ReviewsByProduct";
import ReviewsByUser from "./views/pages/admin/ReviewsByUser";
import ContentPages from "./views/pages/admin/ContentPages";
import ProductBrandLogos from "./views/pages/admin/ProductBrandLogos";
import NewsletterSubscription from "./views/pages/admin/NewsletterSubscription";
import EditContentPage from "./views/pages/admin/EditContentPage";
import AdminOrders from "./views/pages/admin/Orders";
import AdminBrands from "./views/pages/admin/Brands";
import AdminUsers from "./views/pages/admin/Users";
import AdminEditUser from "./views/pages/admin/EditUser";
import AdminModels from "./views/pages/admin/Models";
import AdminYear from "./views/pages/admin/Year";
import AdminTrim from "./views/pages/admin/Trims";
import AdminSettings from "./views/pages/admin/Settings";





let current_url = window.location.hostname;
window.index_url = "";

if (current_url.includes("localhost")) {
  index_url = "/autogig";
}

let metaProgress = {
  func: [{
      call: 'color',
      modifier: 'temp',
      argument: 'red'
    },
    {
      call: 'fail',
      modifier: 'temp',
      argument: 'red'
    },
    {
      call: 'location',
      modifier: 'temp',
      argument: 'top'
    },
    {
      call: 'transition',
      modifier: 'temp',
      argument: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 400
      }
    }
  ]
}

let meta = {
  progress: metaProgress
};

let metaAuthAdmin = {
  requiresAuth: true,
  progress: metaProgress,
  admin: true,
};

let metaAuthUser = {
  requiresAuth: true,
  progress: metaProgress,
  user: true,
};


const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: meta
  },
  {
    path: "/about",
    name: "about",
    component: About,
    meta: meta
  },
  {
    path: "/terms",
    name: "terms",
    component: Terms,
    meta: meta
  },
  {
    path: "/privacy-policy",
    name: "privacypolicy",
    component: PrivacyPolicy,
    meta: meta
  },
  {
    path: "/billing-policy",
    name: "billingpolicy",
    component: BillingPolicy,
    meta: meta
  },
  {
    path: "/reset-password/:code",
    name: "resetpassword",
    component: ResetPassword,
    meta: meta
  },
  {
    path: "/make-payment/:order_code",
    name: "makepayment",
    component: MakePayment,
    meta: meta
  },
  {
    path: "/newsletter",
    name: "newsletter",
    component: NewsLetter,
    meta: meta
  },
  {
    path: "/verify-newsletter-email/:code",
    name: "verifynewsletteremail",
    component: VerifyNewsletterEmail,
    meta: meta
  },
  {
    path: "/autogig-tv",
    name: "autogigtv",
    component: AutogigTv,
    meta: meta
  },
  {
    path: "/fleet-management",
    name: "fleetmanagement",
    component: FleetManagement,
    meta: meta
  },
  {
    path: "/training-certification",
    name: "trainingcertification",
    component: TrainingCertification,
    meta: meta
  },
  {
    path: "/safety-tips",
    name: "safetytips",
    component: SafetyTips,
    meta: meta
  },
  {
    path: "/faq",
    name: "faq",
    component: FAQ,
    meta: meta
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: meta
  },
  {
    path: "/logout",
    name: "logout",
    component: LogOut,
    meta: meta
  },
  {
    path: "/redirect/:page",
    name: "redirect",
    component: Redirect,
    meta: meta
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: meta
  },
  {
    path: "/buy-parts",
    name: "buy_parts",
    component: BuyParts,
    meta: meta
  },
  {
    path: "/add-to-brand",
    name: "add_to_brand",
    component: AddToBrand,
    meta: meta
  },
  {
    path: "/cart",
    name: "cart",
    component: Cart,
    meta: meta
  },
  {
    path: "/checkout",
    name: "checkout",
    component: Checkout,
    meta: meta
  },
  {
    path: "/my-orders",
    name: "myorders",
    component: MyOrders,
    meta: meta
  },
  {
    path: "/change-password",
    name: "changepassword",
    component: ChangePassword,
    meta: metaAuthUser
  },
  {
    path: "/user-profile",
    name: "userprofile",
    component: UserProfile,
    meta: meta
  },
  {
    path: "/contact",
    name: "contact",
    component: Contact,
    meta: meta
  },
  {
    path: "/products",
    name: "products",
    component: Products,
    meta: meta
  },
  {
    path: "/product/:product",
    name: "product_page",
    component: ProductPage,
    meta: meta
  },
  {
    path: "/custom-product/:id/:vin",
    name: "custom_product_page",
    component: CustomProductPage,
    meta: metaAuthUser
  },
  {
    path: "/search",
    name: "search",
    component: Search,
    meta: meta
  },
  {
    path: "/not-found",
    name: "notfound",
    component: NotFound,
    meta: meta
  },
  {
    path: "/admin",
    component: AdminIndex,
    children: [
        {
            path: "",
            name: "admin-dashboard",
            component: AdminDashboard
        },
        {
            path: "products",
            name: "admin-products",
            component: AdminProducts
        },
        {
            path: "category-products/:category_id",
            name: "productsbycategory",
            component: AdminProductsByCategory
        },
        {
            path: "product-categories",
            name: "product-categories",
            component: ProductCategories
        },
        {
            path: "product-brand-logos",
            name: "admin-product-brand-logos",
            component: ProductBrandLogos
        },
        {
            path: "reviews",
            name: "admin-reviews",
            component: AdminReviews
        },
        {
            path: "product-reviews/:product_id",
            name: "reviews-by-product",
            component: ReviewsByProduct
        },
        {
            path: "user-reviews/:user_id",
            name: "reviews-by-user",
            component: ReviewsByUser
        },
        {
            path: "add-product",
            name: "addproduct",
            component: AddProduct
        },
        {
            path: "newsletter-subscription",
            name: "newsletter-subscription",
            component: NewsletterSubscription
        },
        {
            path: "edit-product/:id",
            name: "editproduct",
            component: EditProduct
        },
        {
            path: "orders",
            name: "admin-orders",
            component: AdminOrders
        },
        {
            path: "settings",
            name: "settings",
            component: AdminSettings
        },
        {
            path: "order-page/:order_code",
            name: "orderpage",
            component: OrderPage
        },
        {
            path: "edit-order-page/:id",
            name: "editorderpage",
            component: EditOrderPage
        },
        {
            path: "content-pages",
            name: "contentpages",
            component: ContentPages
        },
        {
            path: "edit-content-page/:id",
            name: "editcontentpage",
            component: EditContentPage
        },
        {
            path: "users",
            name: "admin-users",
            component: AdminUsers
        },
        {
            path: "edit-user/:id",
            name: "edituser",
            component: AdminEditUser
        },
        {
            path: "brands",
            name: "admin-brands",
            component: AdminBrands
        },
        {
            path: "brand-models/:brand",
            name: "admin-models",
            component: AdminModels
        },
        {
            path: "brand-model-years/:brand/:model",
            name: "admin-year",
            component: AdminYear
        },
        {
            path: "brand-model-years-trims/:brand/:model/:year",
            name: "admin-trims",
            component: AdminTrim
        }
    ],
    meta: metaAuthAdmin
  }
];

const router = new VueRouter({
  scrollBehavior(to, from, savedPosition) {
    if(to.query.page == null){
      
      return {
        x: 0,
        y: 0
      }
    }
    
  },
  routes,
  mode: 'history',
  base: index_url
});



let security = {
    guard(to, from, next){
        if (!to.matched.length) {
          next("/not-found");
        }
      
        // check if the route requires authentication and user is not logged in
        if (to.matched.some(route => route.meta.requiresAuth) && !store.getters.isLoggedIn) {
          // redirect to login page
          next({
            name: 'login'
          })
          return
        }
      
        // if logged in redirect to dashboard
        if (to.path === '/login' && store.getters.isLoggedIn) {
          next({
            name: 'home'
          })
          return
        }
      
        
      
        if(to.matched.some(route => route.meta.admin) && store.getters.user.role == "user"){
          next({
            name: 'home'
          })
          return
        }

        next();

    }
}


router.beforeEach((to, from, next) => {

  if(localStorage.getItem('track') == null){
      var length = 10;
      var result           = '';
      var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }

      localStorage.setItem("track", result);
  }
  
  if(store.getters.isLoggedIn && store.getters.user_role == ""){
   
    axios.get(index_url + '/api/user', {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        }
    })
    .then(response => {

        store.commit('loginUser')
        store.commit('user',response.data.user);
        store.commit('user_role',response.data.user.role);


        security.guard(to, from, next);
    })
    .catch(error => {
      security.guard(to, from, next);
    });

  }else{
    security.guard(to, from, next);
  }

  

  
})



export default router
