<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'APIController@register');
Route::post('login', 'APIController@authenticate');

Route::get('get-product-categories', 'APIController@getproductcategories');
Route::get('categories-for-products', 'APIController@categoriesForProducts');
Route::get('get-random-categories', 'APIController@getrandomcategories');
Route::get('get-footer', 'APIController@footer');


Route::post('ql-editor-image-upload', 'APIController@ql_editor_imageupload');
Route::get('get-products', 'APIController@products');

Route::get('get-products-filtered', 'APIController@productsfiltered');

Route::get('get-category-products/{category_id?}', 'APIController@getcategoryproducts');
Route::get('get-product-images/{id?}', 'APIController@getproductimages');
Route::get('get-content-page/{id?}', 'APIController@getcontentpage');
Route::post('send-message', 'APIController@sendmessage');
Route::get('reset-password', 'APIController@resetpassword');
Route::get('logout', 'APIController@logout');


Route::get('get-product/{slug?}', 'APIController@getproduct');





Route::post('reset-password-done', 'APIController@resetpassworddone');
Route::get('verify-newsletter-email-done/{code?}', 'APIController@verifynewsletteremaildone');

Route::get('get-product-brand-logos', 'APIController@getproductbrandlogos');

Route::get('get-home-data', 'APIController@gethomedata');
Route::get('get-brands', 'APIController@getbrands');


Route::get('get-categories-brands-price', 'APIController@get_categories_brands_price');


Route::get('verify-newsletter-email/{email?}', 'APIController@verifynewsletteremail');


Route::get('get-models/{brand?}', 'APIController@getmodels');
Route::get('get-years/{brand?}/{model?}', 'APIController@getyears');
Route::get('get-trims/{brand?}/{model?}/{year?}', 'APIController@gettrims');



Route::get('add-to-normal-cart', 'APIController@addtonormalcart');
Route::post('submit-custom-order', 'APIController@submitcustomorder');

Route::get('get-cart', 'APIController@getcart');

Route::get('update-normal-cart', 'APIController@updatenormalcart');

Route::post('send-order', 'APIController@send_order');

Route::get('get-orders', 'APIController@getorders');

Route::post('payment-successful', 'APIController@payment_successful');

Route::get('get-user-order/{order_code?}', 'APIController@getuserorder');

Route::get('remove-from-normal-cart', 'APIController@removenormalcart');
Route::get('remove-from-custom-cart', 'APIController@removecustomcart');

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::get('user', 'APIController@getAuthenticatedUser');
    Route::post('change-password', 'APIController@changePassword');
    Route::get('dashboard-details', 'APIController@dashboarddetails');


    Route::get('nav-count', 'APIController@navcount');
    Route::get('set-rating', 'APIController@setrating');

    Route::get('get-users', 'APIController@getusers');
    Route::get('delete-user/{id?}', 'APIController@deleteUser');
    Route::get('get-current-user', 'APIController@getcurrentuser');
    Route::get('get-user/{id?}', 'APIController@getuser');
    Route::get('update-user-role/{user_id?}/{role?}', 'APIController@update_user_role');
    Route::post('update-user', 'APIController@updateUser');
    Route::post('update-current-user', 'APIController@updatecurrentuser');

    Route::get('add-brand/{title?}', 'APIController@addBrand');
    Route::get('delete-brand/{id?}', 'APIController@deleteBrand');
    Route::get('edit-brand/{id?}/{new_brand_name?}', 'APIController@editBrand');

    
    Route::get('get-newsletter-emails', 'APIController@getnewsletteremails');
    Route::get('add-newsletter-emails/{emails?}', 'APIController@addnewsletteremails');
    Route::get('edit-newsletter-email/{id?}/{email?}', 'APIController@editnewsletteremail');
    Route::get('delete-newsletter-email/{id?}', 'APIController@deletenewsletteremail');

    Route::get('add-product-category/{title?}', 'APIController@addProductCategory');
    Route::get('delete-product-category/{id?}', 'APIController@deleteProductCategory');
    Route::get('edit-product-category/{id?}/{new_category?}', 'APIController@editProductCategory');

    
    Route::get('add-model/{title?}/{brand_id?}', 'APIController@addModel');
    Route::get('delete-model/{id?}', 'APIController@deleteModel');
    Route::get('edit-model/{id?}/{new_model_name?}', 'APIController@editModel');

    Route::get('add-year/{title?}/{model_id?}', 'APIController@addYear');
    Route::get('delete-year/{id?}', 'APIController@deleteYear');
    Route::get('edit-year/{id?}/{new_year?}', 'APIController@editYear');

    Route::get('add-trim/{title?}/{year_id?}', 'APIController@addTrim');
    Route::get('delete-trim/{id?}', 'APIController@deleteTrim');
    Route::get('edit-trim/{id?}/{new_trim?}', 'APIController@editTrim');



    
    Route::get('delete-product/{id?}', 'APIController@deleteProduct');
    Route::get('delete-product-media', 'APIController@deleteProductMedia');

    
    Route::post('upload-productimages', 'APIController@uploadProductImages');
    Route::post('add-product', 'APIController@addProduct');
    Route::get('edit-product/{id?}', 'APIController@editProduct');
    Route::post('update-product', 'APIController@updateProduct');


    Route::post('upload-product-brand-logos', 'APIController@uploadProductBrandLogos');
    Route::get('delete-product-brand-logo', 'APIController@deleteProductBrandLogo');

    
    Route::get('get-custom-product/{id?}/{vin?}', 'APIController@getcustomproduct');

    Route::get('get-reviews', 'APIController@reviews');
    Route::get('get-review/{review?}', 'APIController@review');
    Route::get('get-product-review/{product_id?}/{review?}', 'APIController@productreview');
    Route::get('get-user-review/{user_id?}/{review?}', 'APIController@userreview');
    Route::get('get-all-orders/{order_type?}', 'APIController@getallorders');
    Route::get('get-order/{order_code?}', 'APIController@getorderid');
    Route::get('edit-order/{id?}', 'APIController@editorderid');
    Route::post('update-order-status', 'APIController@updateOrderStatus');
    Route::post('update-order', 'APIController@updateOrder');

    Route::get('settings', 'APIController@settings');
    Route::get('update-settings', 'APIController@updatesettings');



    
    Route::get('get-custom-orders', 'APIController@getcustomorders');

    Route::get('get-content-pages', 'APIController@getcontentpages');
    Route::post('update-content-page', 'APIController@updatecontentpage');

    

});

