$(function(){
    $("[data-hide]").on("click", function(){
      $(this).closest("." + $(this).attr("data-hide")).hide();
    });

    $('.carousel').carousel({
      interval: 10000,
    });

 
});