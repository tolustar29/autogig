<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $table = 'products_reviews';

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


}
