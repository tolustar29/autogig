<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductMedia extends Model
{
    protected $table = 'products_media';

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

}
