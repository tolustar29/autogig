<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{
    protected $table = 'user_orders';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
