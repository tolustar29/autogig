<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomOrder extends Model
{
    protected $table = 'custom_orders';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
