<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\Model\Product;

class HotproductCount implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value == "no"){
            return true;

        }else{

            $hot = Product::where("hot","yes")->get()->count();

            if($hot < 9){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You can only have 8 hot products.';
    }
}
