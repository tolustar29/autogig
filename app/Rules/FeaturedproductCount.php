<?php

namespace App\Rules;


use App\Model\Product;

use Illuminate\Contracts\Validation\Rule;

class FeaturedproductCount implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value == "no"){
            return true;

        }else{

            $featured = Product::where("featured","yes")->get()->count();

            if($featured < 5){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You can only have 4 featured products.';
    }
}
